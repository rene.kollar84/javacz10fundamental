package cz.java10;

public class IncrementalOperators {
    public static void main(String[] args) {
        int i = 0;
        i = i + 1;//1
        i += 1;//2
        i++;//3
        ++i;//4

        System.out.println(i++);//4
        //i=5
        System.out.println(++i);//6

        //**********
        int j = 10;
        j = j - 1;//9
        j -= 1;//8
        j--;//7
        --j;//6

        System.out.println(j--);//6
        //j=5
        System.out.println(--j);//4
        //j=4
    }
}
