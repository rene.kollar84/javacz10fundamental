package cz.java10;

public class RelationOperators {
    public static void main(String[] args) {
        int a = 10;
        boolean eq = (a == 10);//true
        eq = (a == 20);//false
        eq = (a != 10);//false
        eq = (a != 5);//true

        boolean gt = a > 5;//true
        gt = a < 5;//false

        boolean lt = a < 5;//false
        lt = a < 1000;//true

        boolean leget = a >= 10;//true
        leget = a <= 10;//true

        //true  || true = true
        //false || true = true
        //true  || false = true
        //false || false = false

        //true  && true = true
        //false && true = false
        //true  && false = false
        //false && false = false

        // !true = false
        // !false = true
        // !(true || false && true) = false
        // || vs. |
        // && vs. &

        boolean res = (a < 100) && (a > 5) || (a == 20);//true

        int i = 0;
        boolean res2 = i == 0 || i++ > 0;
        //res2 = true
        //i=0
        System.out.println(i);

        res2 = i == 0 | i++ > 0;
        System.out.println(i);

        i=0;
        res2 = i < 0 && i++ == 20;
        //res2 false
        //i=0

        i=0;
        res2 = i < 0 & i++ == 20;
        //res2 false
        //i=1
    }
}
